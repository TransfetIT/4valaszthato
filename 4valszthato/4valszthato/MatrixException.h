#pragma once
#include <iostream>
#include <string>

class MatrixException 
{
public:
	MatrixException(const std::string arg);
	void printError();
private:
	std::string exception;
};
