#pragma once
#include <GL/glut.h>
class Point
{
public:
		Point();
		Point(double , double );
		friend Point operator-(const Point&, const Point&);
		friend Point operator+(const Point&, const Point&);
		friend Point operator*(const Point&, double);
		friend Point operator/(const Point&, double);
		double x, y;

};