#include <iostream>
#include <cmath>
#include "Matrix.h"


	Matrix::Matrix()
	{

	M = NULL;
	rows = 0;
	cols = 0;
	}

	Matrix::Matrix(int rows , int cols ) : rows(rows), cols(cols)
	{
		if (rows < 1 || cols < 1)
			throw MatrixException("Error in matrix initalization: bad size in constructor.");
		M = NULL;
		M = new double*[rows];
		for (int r = 0; r < rows; r++)
		{
			M[r] = new double[cols];
			for (int c = 0; c < cols; c++)
			{
				M[r][c] = 0;
			}
		}
	}

	Matrix::~Matrix()
	{
		for (int r = 0; r < rows; r++)
		{
			delete M[r];
		}
		delete M;
		M = NULL;
	}

	void Matrix::print()
	{
		if (M != NULL)
		{
			for (int r = 0; r < rows; r++)
			{
				for (int c = 0; c < cols; c++)
					std::cout << M[r][c] << " ";
				std::cout << std::endl;
			}
		}
		else
		{
			std::cout << "Matrix is empty" << std::endl;
		}
	}

	
	Matrix operator+(const Matrix& a, const Matrix& b)
	{
		// check if the dimensions match
		if (a.getRows() == b.getRows() && a.getCols() == b.getCols())
		{
			Matrix res(a.getRows(), a.getCols());

			for (int r = 0; r < a.getRows(); r++)
			{
				for (int c = 0; c < a.getCols(); c++)
				{
					res(r, c) = a(r, c) + b(r, c);
				}
			}
			return res;
		}
		else
		{
			throw MatrixException("Dimensions does not match in matrix addition (operator+)");
		}
	}

	Matrix operator- (const Matrix& a, const Matrix& b)
	{
		// check if the dimensions match
		if (a.getRows() == b.getRows() && a.getCols() == b.getCols())
		{
			Matrix res(a.getRows(), a.getCols());

			for (int r = 0; r < a.getRows(); r++)
			{
				for (int c = 0; c < a.getCols(); c++)
				{
					res(r, c) = a(r, c) - b(r, c);
				}
			}
			return res;
		}
		else
		{
			throw MatrixException("Dimensions does not match in matrix subtraction (operator-)");
		}
	}

	Matrix& Matrix::multiply(Matrix& other)
	{
		if (cols != other.getRows())
			throw MatrixException("Error: Cannot multiply these matrices");

		Matrix* result = new Matrix(rows, other.getCols());

		for (int r = 0; r < rows; r++)
		{
			for (int c = 0; c < other.getCols(); c++)
			{
				double sum = 0;
				for (int i = 0; i < other.getRows(); i++)
				{
					sum = sum + M[r][i] * other(i, c);
				}
				(*result)(r, c) = sum;
			}
		}
		return *result;
	}
	double& Matrix::operator()(const int row, const int col) const
	{
		if (M != NULL && row >= 0 && row < rows && col >= 0 && col < cols)
		{
			return M[row][col];
		}
		else
		{
			throw MatrixException("Out of range exception in Matrix.operator ()");
		}
	}

	Matrix::Matrix(const Matrix& a)
	{
		rows = a.rows;
		cols = a.cols;
		M = new double*[a.getRows()];
		for (int r = 0; r < a.getRows(); r++)
		{
			M[r] = new double[a.getCols()];
			for (int c = 0; c < a.getCols(); c++)
			{
				M[r][c] = a(r, c);
			}
		}
	}

	Matrix operator* (Matrix& a, Matrix& b)
	{
		// check if the dimensions match
		if (a.cols == b.rows)
		{
			Matrix res(a.rows, b.cols);

			for (int r = 0; r < a.rows; r++)
			{
				for (int c_res = 0; c_res < b.cols; c_res++)
				{
					for (int c = 0; c < a.cols; c++)
					{
						res.M[r][c_res] += a.M[r][c] * b.M[c][c_res];
					}
				}
			}
			return res;
		}
		else
		{
			// give an error
			throw MatrixException("Dimensions does not match");
		}

		// return an empty matrix (this never happens but just for safety)
		return Matrix();
	}
	Matrix& Matrix::operator=(const Matrix& a)
	{
		rows = a.getRows();
		cols = a.getCols();
		M = new double*[a.getRows()];
		for (int r = 0; r < a.getRows(); r++)
		{
			M[r] = new double[a.getCols()];

			for (int c = 0; c < a.getCols(); c++)
			{
				M[r][c] = a(r, c);
			}
		}
		return *this;
	}

	Matrix Matrix::getMinor(int row, int col)
	{
		if (row > rows || row < 0 || col > cols || col < 0)
			throw MatrixException("Error: Cannot get minor of matrix: bad row or col index");

		Matrix res = Matrix(rows - 1, cols - 1);

		for (int r = 0; r < rows - 1; r++)
		{
			for (int c = 0; c < cols - 1; c++)
			{
				res(r, c) = M[r + (r >= row)][c + (c >= col)];
			}
		}
		return res;
	}

	double Matrix::determinant()
	{
		double det = 0;
		if (rows == cols)
		{
			if (rows == 1)
			{
				det = M[0][0];
			}
			else if (rows == 2)
			{
				det = M[0][0] * M[1][1] - M[1][0] * M[0][1];
			}
			else
			{
				for (int c = 0; c < cols; c++)
				{
					Matrix newMatrix = this->getMinor(0, c);
					det += pow(-1, c) * M[0][c] * newMatrix.determinant();
				}
			}
		}
		else
		{
			throw MatrixException("Matrix must be square to calculate determinant!");
		}
		return det;
	}

	Matrix Matrix::invert()
	{
		if (this->determinant() == 0 || (rows != cols))
		throw MatrixException("Error: cannot invert matrix: determinant is 0 or not a square matrix");

		Matrix res = Matrix(rows, cols);
		for (int r = 0; r < rows; r++)
		{
			for (int c = 0; c < cols; c++)
			{
				res(r, c) = this->getMinor(r, c).determinant();
			}
		}
		res = res.transpose();
		double det = this->determinant();

		for (int r = 0; r < rows; r++)
		{
			for (int c = 0; c < cols; c++)
			{
				res(r, c) *= (r + c) % 2 == 0 ? 1 : -1;
				res(r, c) /= det;
			}
		}
		return res;
	}

	Matrix Matrix::transpose()
	{
		if (rows != cols)
			throw MatrixException("Error: Cannot transpose not square matrix!");
		Matrix res = Matrix(rows, cols);

		for (int r = 0; r < rows; r++)
		{
			for (int c = 0; c < cols; c++)
			{
				res(r, c) = M[c][r];
			}
		}
		return res;
	}


	int Matrix::getCols() const
	{
		return cols;
	}
	int Matrix::getRows() const
	{
		return rows;
	}