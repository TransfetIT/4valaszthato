#include "point.h"

Point::Point()
{
	double x, y;
}

Point::Point(double x0, double y0)
{
	x = x0;
	y = y0;
}

Point operator-(const Point&a, const Point&b)
{

	Point p;
	p.x = a.x - b.x;
	p.y = a.y - b.y;

	return p;
}

Point operator+(const Point&a, const Point&b)
{
	Point p;
	p.x = a.x + b.x;
	p.y = a.y + b.y;

	return p;
}

Point operator*(const Point&a, double i)
{
	Point p;
	p.x = a.x*i;
	p.y = a.y*i;

	return p;
}

Point operator/(const Point&a, double n)
{
	Point p;
	p.x = a.x / n;
	p.y = a.y / n;

	return p;
}