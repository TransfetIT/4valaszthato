#include "Matrix.h"
#include "point.h"
#include <GL\glut.h>
#include <vector>


const int winWidth = 1000, winPositionX = 100;
const int winHeight = 700, winPositionY = 100;
int dragged = -1;
int draggedType = -1;
int n = 4;
			
std::vector<Point> hermitPoints = { Point(300,300) ,Point(290, 600) ,Point(450, 580) ,Point(570,200) };
std::vector<Point> bezierPoints = { Point(500,440), Point(350,400) };


Matrix r1 = Matrix(2, 1);		//t1 v�gpontbeli �rint�vektor P3 b�l mutat a P4 be
Matrix r2 = Matrix(2, 1);		//r2 kezd�pontbeli �rint�vektor
Matrix G;						// 3 pont + 1 �rint�vektor
Matrix M;						// M m�trix T inverze
Matrix T;						// kirajzol�sn�l
Matrix T1;						// T1 m�trixb�l sz�rmaztatjuk az M m�trixot

Matrix S = Matrix(1, 3);		//s�lyok, t1,t2,t3
Matrix C = Matrix(2, 4);		
float t1 = -2;
float t2 = 0.3;
float t3 = 1.5;


Matrix Tv = Matrix(4, 1);


// Bezier-g�rbe kontrollpontok, adatok


void mainWindowInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);					// Set display-window color to white
	glMatrixMode(GL_PROJECTION);						// Set projection parameters.
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(10.0);
	gluOrtho2D(0.0, winWidth, 0.0, winHeight);			// Set a 2D ortographic projection matrix
}

void refreshG()
{
	r1(0, 0) = hermitPoints.at(3).x - hermitPoints.at(2).x;
	r1(1, 0) = hermitPoints.at(3).y - hermitPoints.at(2).y;

	G(0, 0) = hermitPoints.at(0).x;
	G(1, 0) = hermitPoints.at(0).y;

	G(0, 1) = hermitPoints.at(1).x;
	G(1, 1) = hermitPoints.at(1).y;

	G(0, 2) = hermitPoints.at(2).x;
	G(1, 2) = hermitPoints.at(2).y;

	G(0, 3) = r1(0, 0);
	G(1, 3) = r1(1, 0);
}


void HermitCurve()
{
	refreshG();
	C = G*M;

	glColor3d(1.0, 0.0, 0.0);
	glBegin(GL_LINE_STRIP);
	for (float i =	-2; i <= 1.5; i += 0.05)
	{
		Matrix Akt = Matrix(2, 1);
		T(0, 0) = i*i*i;
		T(1, 0) = i*i;
		T(2, 0) = i;
		T(3, 0) = 1;
		Akt = C*T;

			glVertex2f(Akt(0, 0), Akt(1, 0));
	}
	glEnd();

	glColor3d(0.0, 1.0, 1.0);
	glBegin(GL_POINTS);
	glVertex2d(G(0, 0), G(1, 0));
	glVertex2d(G(0, 1), G(1, 1));
	glVertex2d(G(0, 2), G(1, 2));
	glVertex2d(hermitPoints.at(3).x, hermitPoints.at(3).y);
	glEnd();
	


	glColor3d(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex2d(hermitPoints.at(2).x,hermitPoints.at(2).y);
	glVertex2d(hermitPoints.at(3).x,hermitPoints.at(3).y);
	glEnd();
	

}

void BezierCurve()
{

	refreshG();
	C = G*M;
	r2 = C*Tv;

	Point Q0 = hermitPoints.at(2);		//kezd�pont a hermit�v v�gpontja
	Point Q5 = hermitPoints.at(0);		//v�gpont a hermit�v kezd�pontja

	Point Q1 = (hermitPoints.at(3) - hermitPoints.at(2) + Q0*n) / n;
	Point Q4;

	Point kezdoErinto = Point(r2(0, 0), r2(1, 0));

	Q4 = ((Q5*n - kezdoErinto)/n);
	std::cout << Q4.x << "    " << Q4.y << std::endl;
	glColor3d(0.0, 0.0, 1.0);
	glBegin(GL_LINE_STRIP);

	glVertex2d(Q0.x, Q0.y);
	glVertex2d(Q1.x, Q1.y);
	glVertex2d(bezierPoints.at(0).x, bezierPoints.at(0).y);
	glVertex2d(bezierPoints.at(1).x, bezierPoints.at(1).y);
	glVertex2d(Q4.x, Q4.y);
	glVertex2d(Q5.x, Q5.y);

	glEnd();

	glColor3d(1.0, 1.0, 1.0);
	glBegin(GL_POINTS);
	glVertex2d(Q0.x, Q0.y);
	glVertex2d(Q1.x, Q1.y);
	glVertex2d(bezierPoints.at(0).x, bezierPoints.at(0).y);
	glVertex2d(bezierPoints.at(1).x, bezierPoints.at(1).y);
	glVertex2d(Q4.x, Q4.y);
	glVertex2d(Q5.x, Q5.y);
	glEnd();


	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	for (GLfloat t = 0; t <= 1; t += 0.001)
	{
		glVertex2f(
			((1 - t)*(1 - t)*(1 - t)*(1 - t)*(1 - t)) * Q0.x +
			(5 * t*((1 - t)*(1 - t)*(1 - t)*(1 - t))) * Q1.x +
			(5 * t*t*(1 - t)*(1 - t)*(1 - t))*bezierPoints.at(0).x +
			(5 * t*t*t*(1 - t)*(1 - t))*bezierPoints.at(1).x +
			(5 * t*t*t*t*(1 - t))*Q4.x +
			(t*t*t*t*t)*Q5.x
			,
			((1 - t)*(1 - t)*(1 - t)*(1 - t)*(1 - t)) * Q0.y +
			(5 * t*((1 - t)*(1 - t)*(1 - t)*(1 - t))) * Q1.y +
			(5 * t*t*(1 - t)*(1 - t)*(1 - t))*bezierPoints.at(0).y +
			(5 * t*t*t*(1 - t)*(1 - t))*bezierPoints.at(1).y +
			(5 * t*t*t*t*(1 - t))*Q4.y +
			(t*t*t*t*t)*Q5.y);
	}
	glEnd();


	glutPostRedisplay();
}

void mainDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	HermitCurve();
	BezierCurve();

	glutSwapBuffers();
}

void refreshT1()
{
	std::cout << " Refresh Called" << std::endl;
	for (int c = 0; c < T1.getCols(); c++)
	{
		std::cout << c << std::endl;
		if (c == 3)
			T1(0, c) = 3 * S(0, 2)*S(0, 2);
		else
			T1(0, c) = S(0, c) * S(0, c) * S(0, c);
	}
	for (int c = 0; c < T1.getCols(); c++)
	{
		if (c == 3)
			T1(1, c) = 2 * S(0, 2);
		else
			T1(1, c) = S(0, c) * S(0, c);
	}
	for (int c = 0; c < T1.getCols(); c++)
	{
		if (c == 3)
			T1(2, c) = 1;
		else
			T1(2, c) = S(0, c);	
	}
	for (int c = 0; c < T1.getCols(); c++)
	{
		if (c == 3)
			M(3, c) = 0;
		else
			T1(3, c) = 1;	
	}

	T1.print();
}

void processMouse(GLint Button, GLint state, GLint xMouse, GLint yMouse)
{
	int sensitivity = 10;
	if (Button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
	
		for (int i = 0; i < hermitPoints.size(); i++)
		{
			std::cout << hermitPoints.at(i).x <<"xxxx"<< std::endl;
			std::cout << hermitPoints.at(i).y <<"yyyy"<< std::endl;
			if (hermitPoints.at(i).x - xMouse < sensitivity && fabs(winHeight - hermitPoints.at(i).y - yMouse) < sensitivity)
			{
				std::cout << "dragged" << std::endl;
				dragged = i;
				draggedType = 0;
			}
		}

		for (int i = 0; i < bezierPoints.size(); i++)
		{
			if (bezierPoints.at(i).x - xMouse < sensitivity && fabs(winHeight - bezierPoints.at(i).y - yMouse) < sensitivity)
			{
				std::cout << "dragged" << std::endl;
				dragged = i;
				draggedType = 2;
			}
		}

	}

	if (Button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		dragged = -1;
	}

}

void processMouseActiveMotion(GLint xMouse, GLint yMouse)
{
	std::cout << xMouse << std::endl;
	std::cout << yMouse << std::endl;
	if (dragged > -1)
	{
		if (draggedType == 0)
		{
			std::cout << " v�lt" << std::endl;
			hermitPoints.at(dragged).x = xMouse;
			hermitPoints.at(dragged).y = winHeight - yMouse;
		}
		else if (draggedType == 2)
		{
			std::cout << " v�lt" << std::endl;
			bezierPoints.at(dragged).x = xMouse;
			bezierPoints.at(dragged).y = winHeight - yMouse;
		}
	}

	glutPostRedisplay();
}


int main(int argc, char** argv)
{

	G = Matrix(2, 4);		// G geometriai adatok , 3 pont +1 �rint�vektor		
	T = Matrix(4, 1);		//T m�trix a kirajzol�sn�l folyamatosan v�ltozik
	M = Matrix(4, 4);		//M m�trix a s�lyokb�l el��ll�tott m�trix
	//s�lyvektor
	S(0, 0) = t1;			
	S(0, 1) = t2;
	S(0, 2) = t3;


	//T oszlopvektor

	T(0, 0) = 1;			
	T(1, 0) = 1;
	T(2, 0) = 1;
	T(3, 0) = 1;
	
	//G= (P1,P2,P3,R4)		// R4 a P3 b�l sz�rmaztatott �rint�vektor

	Tv(0, 0) = 3 * t1*t1;
	Tv(1, 0) = 2 * t1;
	Tv(2, 0) = 1;
	Tv(3, 0) = 0;
	

	T1 = Matrix(4, 4);

	refreshT1();
	T1.print();
	M = T1.invert();
	

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(winWidth, winHeight);
	glutInitWindowPosition(winPositionX, winPositionY);
	glutCreateWindow("HermitAndBezier");

	mainWindowInit();

	refreshG();
	C = G*M;

	r2 = C*Tv;
	r2.print();

	glutDisplayFunc(mainDisplay);
	glutMouseFunc(processMouse);
	glutMotionFunc(processMouseActiveMotion);

	glutMainLoop();
}