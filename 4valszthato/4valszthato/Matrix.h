#pragma once

#include <iostream>
#include "MatrixException.h"

class Matrix
{
public:

	Matrix();
	Matrix(int , int );
	~Matrix();
	void print();
	friend Matrix operator+(const Matrix& , const Matrix& );
	friend Matrix operator- (const Matrix& , const Matrix& );
	Matrix& multiply(Matrix& );
	double& operator()(const int , const int ) const;
	Matrix(const Matrix& a);
	friend Matrix operator* (Matrix& , Matrix& );
	Matrix& operator= (const Matrix& );
	Matrix getMinor(int , int );
	double determinant();
	Matrix invert();

	Matrix transpose();

	int getCols() const;
	int getRows() const;
private:
	int rows;
	int cols;
	double** M;
};